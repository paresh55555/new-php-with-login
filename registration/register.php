<?php include('server.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login </title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="row">
    <div class='container col-md-2 col-md-offset-5'  >
        <div class="header">
            <h1>Register</h1>
        </div>
            <form action="register.php" method="post">
                <?php include('error.php') ?>
                <div class="input-group" class="form-control">
                    <label for="">UserName</label>
                </div>
                <div class="input-group">
                    <input type="text" class="form-control" name="username" value= "<?php echo $username; ?>">
                </div>
                <div class="input-group" class="fo">
                    <label for="">Password</label>
                </div>
                <div class="input-group">
                    <input type="password" class="form-control" name="password_1" value=  ''>
                </div>
                <div class="input-group" class="form-control">
                  <label for="">Confirm Password</label>  
                </div>
                <div class="input-group" class="form-control">
                    <input type="password" class="form-control" name="password_2" value ="">
                </div>
                <div class="input-group" class="form-control">
                    <label for="">email</label>
                </div>
                <div class="input-group">
                    <input type="email" class="form-control" name="email" value= "<?php echo $email; ?>">
                </div>
                <p></p>
                <div class="input-group">
                    <button type="submit" class="form-control" name="register" class="btn">Register</button>
                </div>
                <p></p>
                <p>
                    ALREADY Member ? <a href="login.php">sing in</a>
                </p>
                
        
            </form>
    </div>
</div>

</body>
</html>